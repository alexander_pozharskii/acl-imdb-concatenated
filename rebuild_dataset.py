import os
import pandas as pd
from tqdm import tqdm


PATHS = ["C:\\Users\\gauss\\Downloads\\aclImdb_v1.tar\\aclImdb_v1\\aclImdb\\test",
         "C:\\Users\\gauss\\Downloads\\aclImdb_v1.tar\\aclImdb_v1\\aclImdb\\train"]
DS_PATHS = ["test.csv", "train.csv"]
PREFIXES = ["test", "train"]


def sentiment_df(path, sentiment, prefix):
    files = os.listdir(os.path.join(path, sentiment))
    texts = []
    for file in tqdm(files, desc='{0} - {1}'.format(sentiment, prefix)):
        with open(os.path.join(path, sentiment, file), 'r', encoding='utf-8') as src:
            text = src.read()
        texts.append(text)
    with open(os.path.join(path, 'urls_{0}.txt'.format(sentiment)), 'r', encoding='utf-8') as src:
        urls = src.readlines()
    return pd.DataFrame({
        'text': texts,
        'url': urls,
        'sentiment': [sentiment] * len(texts)
    })


if __name__ == '__main__':
    for path, ds_path, prefix in zip(PATHS, DS_PATHS, PREFIXES):
        sentiments = [fname
                      for fname in os.listdir(path)
                      if os.path.isdir(os.path.join(path, fname))]
        sentiment_dfs = [sentiment_df(path, sentiment, prefix)
                         for sentiment in sentiments]
        pd.concat(sentiment_dfs).to_csv(ds_path, index=False)
